#!/bin/bash

if ! command -v python3 &> /dev/null
then
    echo "python3 could not be found, please install then run this script again"
    exit
fi

filename="results-$(date +%s).txt"

touch $filename

echo -n "Enter ISP type (cell or residential): "
read isp

echo -n "Enter service provider (n/a accepted for privacy: "
read provider

echo -n "Enter geographic location (n/a accepted for privacy): "
read geo

cat >> $filename <<EOF
ISP Type: $isp
Service Provider: $provider
Geo Location: $geo

EOF


counter=1
while [ $counter -le 10 ]
do
echo Test# $counter >> $filename
python3 pynat.py | tee --append $filename
((counter++))
done

echo Results stored in $filename