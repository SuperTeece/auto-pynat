# Automated PyNAT
Many of us are behind NAT, but what type and how does it behave? A Python project called [PyNAT](https://github.com/aarant/pynat) uses public STUN servers to reveal what type of NAT is currently in use. Just goofing around I discovered that my residential ISP has me behind a restricted-cone NAT at first, but if I repeat the request consistently the NAT type is changed to full-cone and remains that way so long as I continue to run the test.

Going deeper into the rabbit hole I tried the same with my cellphone's hotspot. The behavior is similar, except the full-cone's timeout is much faster. I might see one or two full-cone results when running PyNAT tests back to back.

So I wrote this script to automate the experiment a bit. The Bash script sets up a results file, executes PyNAT ten times, and writes the results in the file. This project contains a modified PyNAT Python script. The modification is to redact IP addresses from the reported results.